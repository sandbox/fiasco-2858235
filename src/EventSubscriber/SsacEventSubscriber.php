<?php

namespace Drupal\lockdown\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber EAEventSubscriber.
 */
class SsacEventSubscriber implements EventSubscriberInterface {

  /**
   * Add a "Vary: X-SSAC" header on every page.
   *
   * @param FilterResponseEvent $event
   *    The event object containing the response.
   */
  public function addVaryHeader(FilterResponseEvent $event) {
    $response = $event->getResponse();
    $response->headers->set('Vary', 'X-SSAC');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['addVaryHeader'];
    return $events;
  }

}
