# Installation

## Modify .htaccess
Add this to the bottom of your .htaccess file:

```
Header always append Vary X-SSAC
SetEnvIf X-SSAC "<secret>" AuthorisedRequest=1

Order allow,deny
Allow from env=AuthorisedRequest
Deny from all
```

This will do the actual access control to Drupal. The Vary header both in apache and in Drupal are
purely to retain valid caching through a reverse proxy such as Varnish.
